package ictgradschool.industry.lab12.bounce;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

public class ImageShape extends Shape {
	String imageFile;
	BufferedImage image;

	public ImageShape(String file) {
		super();
		imageFile = file;
	}

	public ImageShape(int x, int y, String file) {
		super(x,y);
		imageFile = file;
	}

	public ImageShape(int x, int y, int deltaX, int deltaY, String file) {
		super(x,y,deltaX,deltaY);
		imageFile = file;
	}

	public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String file) {
		super(x,y,deltaX,deltaY,width,height);
		imageFile = file;
	}

	public void createImage(){
		try {
			image = ImageIO.read(new File("cat.jpg"));
		} catch(IOException e){
			System.out.println(e);
		}
	}
	
	public void paint(Painter painter) {
		createImage();
		//Rectangle frame = painter.drawRect(fX, fY, fWidth, fHeight);
		painter.drawImage(image, fX, fY, fWidth, fHeight, null);

	}
}
