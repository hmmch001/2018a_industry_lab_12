package ictgradschool.industry.lab12.bounce;

import java.awt.*;

public class DynamicRectangleShape extends Shape {
	Color fill = Color.black;
	int shapeVersion = 1;
	int previousDeltaX;
	int previousDeltaY;



	public DynamicRectangleShape() {
		super();
	}

	public DynamicRectangleShape(Color c) {
		super();
		fill = c;
	}

	public DynamicRectangleShape(int x, int y, Color c) {
		super(x,y);
		fill = c;
	}

	public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, Color c) {
		super(x,y,deltaX,deltaY);
		fill = c;
	}

	public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Color c) {
		super(x,y,deltaX,deltaY,width,height);
		fill = c;
	}

	public void changeShape(){

		if(previousDeltaX != fDeltaX){
			shapeVersion = 2;
		}else if(previousDeltaY != fDeltaY){
			shapeVersion = 3;
		}

	}

	public void move(int width, int height) {
		previousDeltaX = fDeltaX;
		previousDeltaY = fDeltaY;
		super.move(width, height);
		changeShape();
	}

	
	public void paint(Painter painter) {
		if(shapeVersion == 1){
			painter.setColor(Color.pink);
			painter.fillRect(fX,fY,fWidth,fHeight);
		}else if(shapeVersion == 2){
			painter.setColor(fill);
			painter.fillRect(fX,fY,fWidth,fHeight);
		}else if(shapeVersion == 3){
			painter.setColor(Color.black);
			painter.drawRect(fX,fY,fWidth,fHeight);
		}



	}
}
