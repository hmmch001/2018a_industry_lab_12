package ictgradschool.industry.lab12.bounce;

import java.awt.*;
import java.lang.reflect.Array;

public class GemShape extends Shape {

	public GemShape() {
		super();
	}

	public GemShape(int x, int y) {
		super(x,y);
	}

	public GemShape(int x, int y, int deltaX, int deltaY) {
		super(x,y,deltaX,deltaY);
	}

	public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
		super(x,y,deltaX,deltaY,width,height);
	}
	
	public void paint(Painter painter) {
		int [] xPoints;
		int [] yPoints;
		int numPoints;
		if(fHeight <= 40){
			numPoints = 4;
			xPoints = new int[]{fX+(fWidth/2), fX+fWidth, fX+(fWidth/2), fX};
			yPoints = new int[]{fY, fY+(fHeight/2), fY+fHeight, fY+(fHeight/2)};
		}else{
			numPoints = 6;
			xPoints = new int[]{fX+20, fX+(fWidth-20), fX+fWidth, fX+(fWidth-20),fX+20, fX };
			yPoints = new int[]{fY, fY, fY+(fHeight/2), fY+fHeight, fY+fHeight, fY+(fHeight/2)};
		}
		Polygon gem = new Polygon(xPoints,yPoints, numPoints);
		painter.drawPolygon(gem);
	}
}
